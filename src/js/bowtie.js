let forwardTimes = [];

async function onPlay() {
  const videoEl = $("#inputVideo").get(0);

  if (videoEl.paused || videoEl.ended || !isFaceDetectionModelLoaded())
    return setTimeout(() => onPlay());

  const options = getFaceDetectorOptions();
  const result = await faceapi.detectSingleFace(videoEl, options);

  if (result) {
    const canvas = $("#overlay").get(0);
    const img = document.getElementById("mask");
    const dims = faceapi.matchDimensions(canvas, videoEl, true);
    // faceapi.draw.drawDetections(canvas, faceapi.resizeResults(result, dims));
    const { box } = faceapi.resizeResults(result, dims);
    const { _x, _y, _width, _height } = box;

    const left = _x + _width * 0.2;
    const top = _y + _height * 0.8;
    const width = _width * 0.6;
    const height = _height * 0.4;

    img.style.position = "absolute";
    img.style.left = `${left}px`;
    img.style.top = `${top}px`;
    img.style.width = `${width}px`;
    img.style.height = `${height}px`;
  }

  setTimeout(() => onPlay());
}

async function run() {
  // load face detection model
  await changeFaceDetector(TINY_FACE_DETECTOR);
  changeInputSize(320);

  // try to access users webcam and stream the images
  // to the video element
  const stream = await navigator.mediaDevices.getUserMedia({ video: {} });
  const videoEl = $("#inputVideo").get(0);
  videoEl.srcObject = stream;
}

$(document).ready(function () {
  initFaceDetectionControls();
  run();
});
